{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
module Main where

import Relude
import Servant
--import Servant.Client
--import Servant.Server
import Data.Aeson
import qualified Data.Aeson as JSON
--import Network.Wai
import Network.Wai.Handler.Warp
import Data.Time

data Booking = Booking
  {
    bookingID :: Maybe ID
    ,bookingFrom :: UTCTime
    ,environmentID :: Maybe ID
  }
  deriving (Eq,Show,Generic)

data DB = DB {
  environments :: TVar [Environment]
  , lastEnvIdx :: TVar Int
  , bookings :: TVar [Booking]
  , lastBookingIdx :: TVar Int
}

type ID = String

data Environment = Environment {
  envID :: Maybe ID
  , name :: String
}
   deriving (Eq, Show, Generic)

nullEnvironment :: Environment
nullEnvironment = Environment { envID = Nothing, name = ""}

type EnvironmentAPI =
  "environments" :> Get '[JSON] [Environment] -- display
  :<|>  "environments" :> ReqBody '[JSON] Environment :> PostCreated '[JSON] Environment -- Create new
  :<|> "environments" :> Capture "id" String :> Get '[JSON] Environment -- Show individual through respective search
  :<|> "bookings" :> Get '[JSON] [Booking]
  :<|> "environments" :> Capture "env_id" String :> "bookings" :> ReqBody '[JSON] Booking :> PostCreated '[JSON] Booking

type AppM = ReaderT DB Handler

instance ToJSON Environment
instance FromJSON Environment

instance ToJSON Booking
instance FromJSON Booking where
  parseJSON (Object v) =
    Booking Nothing Nothing <$> v .: "booking_from"
--       <*> v .: "envID"
  parseJSON _ = mzero

inc :: Int -> Int
inc = (+1)

createEnvironment :: Environment -> AppM Environment
createEnvironment e = do
  DB {environments = p, lastEnvIdx = q} <- ask
  i <- readTVarIO q
  let e' = e {envID = Just $ show i}
  liftIO $ atomically $ readTVar p >>= writeTVar p . (e' : )
  liftIO $ atomically $ readTVar q >>= writeTVar q . inc --(e {envID = i} : )
  return e

listEnvironments :: AppM [Environment]
listEnvironments = do
  DB {environments = p} <- ask
  liftIO $ readTVarIO p

createBooking :: String -> Booking -> AppM Booking
createBooking envId b = do
  DB {environments = o, bookings = p, lastBookingIdx = q} <- ask
  envs <- readTVarIO o
  case find (\x -> envID x == Just envId) envs of
    Nothing -> throwError err404 {errBody = JSON.encode $ "Environment with ID " ++ envId ++ " not found."}
    Just e -> do
      i <- readTVarIO q
      let b' = b {bookingID = Just $ show i}
      liftIO $ atomically $ readTVar p >>= writeTVar p . (b' : )
      liftIO $ atomically $ readTVar q >>= writeTVar q . inc --(e {envID = i} : )
      return b'

getEnvironment :: String -> AppM Environment
getEnvironment i = do
  DB {environments = p} <- ask
  environments <- readTVarIO p
  case  (find (\x -> envID x == Just i) environments) of
    Just e -> return e
    Nothing -> throwError err404 {errBody = JSON.encode $ "Environment with ID " <> i <> " not found."}

listBookings :: AppM [Booking]
listBookings = do
  DB {bookings = p} <- ask
  liftIO $ readTVarIO p

server :: ServerT EnvironmentAPI AppM
server = listEnvironments  :<|> createEnvironment :<|> getEnvironment :<|> listBookings :<|> createBooking

--server1 :: Server UserAPI1
--server1 = return users1 :<|> singleUser

userAPI :: Proxy EnvironmentAPI
userAPI = Proxy

--singleUser = undefined
  -- return $ fromMaybe (User "" 0 "") (find (\x -> name x == name') users1)

--app1 :: Application
--app1 = serve userAPI server

nt :: DB -> AppM a -> Handler a
nt s x = runReaderT x s

app :: DB -> Application
app s = serve userAPI $ hoistServer userAPI (nt s) server

main :: IO ()
main = do
  putStrLn "running..."
  initialEnvs <- newTVarIO  []
  intialIdx <- newTVarIO 0
  initialBookings <- newTVarIO []
  initialBookingIdx <- newTVarIO 0
  run 8081 $ app $ DB initialEnvs intialIdx initialBookings initialBookingIdx

  --run 8081 app1
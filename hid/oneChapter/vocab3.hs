{-# LANGUAGE OverloadedStrings #-}

import Data.List (group,sort,sortOn,sortBy)
import Data.Char
import Data.Ord
import qualified Data.Text as T
import Data.Text (Text)

type Entry = (T.Text,Int)

type Vocabulary = [Entry]

extractVocab :: Text -> Vocabulary
extractVocab  = map (\xs -> (head xs,length xs)) . group . sort . map T.toCaseFold . filter (not . T.null) .  map (T.dropAround $ not . isLetter )  .  T.words

allWordsReport :: Vocabulary -> Text
allWordsReport = T.unwords  . allWords

wordsCountReport :: Vocabulary -> Text
wordsCountReport es =  T.unlines [part1,part2]
  where (total,unique)  = wordsCount es
        part1 = T.append  "Total no.of words: " (T.pack $ show total)
        part2 = T.append  "unique no.of words: " (T.pack $ show unique)

frequentWordsReport :: Vocabulary -> Int -> Text
frequentWordsReport es n = T.unlines . map fst $ take n $ wordsByFrequency es

processTextFile :: FilePath -> Bool -> Int -> IO ()
processTextFile = undefined

allWords :: Vocabulary -> [Text]
allWords = map fst  

wordsCount :: Vocabulary -> (Int,Int)
wordsCount es = (total,unique)
  where total = sum . map snd $ es
        unique = length es

wordsByFrequency :: Vocabulary -> Vocabulary
wordsByFrequency = sortBy (comparing $ Down . snd)

main :: IO ()
main = undefined

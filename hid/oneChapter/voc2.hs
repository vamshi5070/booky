import Data.Char
import Data.List (group,sort)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import System.Environment

type Entry = (T.Text,Int)

type Vocabulary = [Entry]

extractVocab :: T.Text -> Vocabulary
extractVocab  = map (\xs -> (head xs,length xs)) . group . sort . map T.toCaseFold . filter (not . T.null) .  map (T.dropAround $ not . isLetter )  .  T.words

  -- (\xs -> (xs,length xs)) <$> group $ sort $ map T.toCaseFold $ filter (not . T.null) $ map (T.dropAround $ not . isLetter) $ T.words t
-- T.words t
-- extractVocab = undefined

printAllWords :: Vocabulary -> IO()
printAllWords vocab = do
  putStrLn "All words: "
  TIO.putStrLn . T.unlines $ map fst vocab
-- undefined
processTextFile :: FilePath -> IO ()
processTextFile fname = do
  text <- TIO.readFile fname
  let vocab = extractVocab text
  printAllWords vocab


main :: IO ()
main = do
  putStr "process starts...."
  args <- getArgs
  case args of
    [fname] -> processTextFile fname
    _ -> putStrLn "Usage: vocab-builder filename"

    

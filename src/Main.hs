{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
    
module Main where

import Database.PostgreSQL.Simple
--import Servant.Client
--import Servant.Server
import Data.Aeson
import qualified Data.Aeson as JSON
--import Network.Wai

import Data.Time
import Network.Wai.Handler.Warp
import Relude
import Servant

data Booking = Booking
  { bookingID :: Maybe ID,
    bookingEnvID :: Maybe ID,
    bookingFrom :: UTCTime
  }
  deriving (Eq, Show, Generic)

data DB = DB
  { environments :: TVar [Environment],
    lastEnvIdx :: TVar Int,
    bookings :: TVar [Booking],
    lastBookingIdx :: TVar Int
  }

type ID = String

data Environment = Environment
  { envID :: Maybe ID,
    name :: String
  }
  deriving (Eq, Show, Generic)

nullEnvironment :: Environment
nullEnvironment = Environment {envID = Nothing, name = ""}

type EnvironmentAPI = GetEnvAPI
 :<|> CreateEnvAPI
 {-
 :<|> GetOneEnvAPI 
 :<|> GetBookingAPI 
 :<|> CreateBookAPI 
-}

server :: ServerT EnvironmentAPI AppM
server = listEnvironments :<|> createEnvironment -- :<|> getEnvironment :<|> listBookings :<|> createBooking

type GetEnvAPI =  "environments" :> Get '[JSON] [Environment]
type CreateEnvAPI = "environments" :> ReqBody '[JSON] Environment :> PostCreated '[JSON] Environment
type GetOneEnvAPI = "environments" :> Capture "id" String :> Get '[JSON] EnvironmentWithBooking
type GetBookingAPI = "bookings" :> Get '[JSON] [Booking]
type CreateBookAPI = "environments" :> Capture "env_id" String :> "bookings" :> ReqBody '[JSON] Booking :> PostCreated '[JSON] Booking
-- :<|>

type AppM = ReaderT DB Handler

instance ToJSON Environment
instance ToJSON EnvironmentWithBooking

instance FromJSON Environment

instance ToJSON Booking

instance FromJSON Booking where
  parseJSON (Object v) =
    Booking Nothing Nothing <$> v .: "booking_from"
  --       <*> v .: "envID"
  parseJSON _ = mzero

data EnvironmentWithBooking = EnvironmentWithBooking
  {
    envEnvID :: ID
    , envName :: String
    , envBookings :: [Booking]
  }
  deriving (Eq, Show, Generic)

envWithBookings :: Environment -> AppM EnvironmentWithBooking
envWithBookings e = do
  DB {bookings = p} <- ask
  allBookings <- readTVarIO p
  let bs = filter (\b -> bookingEnvID b == envID e) allBookings
      sortedBs = sortOn (fromMaybe "" . bookingID) bs
  return $ EnvironmentWithBooking (fromMaybe "" $ envID e) (name e) sortedBs  -- (sortBy (\b b' -> fromMaybe "" bookingID b) bs) 

inc :: Int -> Int
inc = (+ 1)

createEnvironment :: Environment -> AppM Environment
createEnvironment e = do
  DB {environments = p, lastEnvIdx = q} <- ask
  i <- readTVarIO q
  let e' = e {envID = Just $ show i}
  liftIO $ atomically $ readTVar p >>= writeTVar p . (e' :)
  liftIO $ atomically $ readTVar q >>= writeTVar q . inc --(e {envID = i} : )
  return e

listEnvironments :: AppM [Environment]
listEnvironments = do
  DB {environments = p} <- ask
  liftIO $ readTVarIO p

createBooking :: String -> Booking -> AppM Booking
createBooking envId b = do
  DB {environments = o, bookings = p, lastBookingIdx = q} <- ask
  envs <- readTVarIO o
  case find (\x -> envID x == Just envId) envs of
    Nothing -> throwError err404 {errBody = JSON.encode $ "Environment with ID " ++ envId ++ " not found."}
    Just _ -> do
      i <- readTVarIO q
      let b' = b {bookingID = Just $ show i, bookingEnvID = Just envId}
      liftIO $ atomically $ readTVar p >>= writeTVar p . (b' :)
      liftIO $ atomically $ readTVar q >>= writeTVar q . inc --(e {envID = i} : )
      return b'

getEnvironment :: String -> AppM EnvironmentWithBooking
getEnvironment i = do
  DB {environments = p} <- ask
  envs <- readTVarIO p
  case (find (\x -> envID x == Just i) envs) of
    Just e ->
       envWithBookings e
    Nothing -> throwError err404 {errBody = JSON.encode $ "Environment with ID " <> i <> " not found."}

listBookings :: AppM [Booking]
listBookings = do
  DB {bookings = p} <- ask
  liftIO $ readTVarIO p

--server1 :: Server UserAPI1
--server1 = return users1 :<|> singleUser

userAPI :: Proxy EnvironmentAPI
userAPI = Proxy

--singleUser = undefined
-- return $ fromMaybe (User "" 0 "") (find (\x -> name x == name') users1)

--app1 :: Application
--app1 = serve userAPI server

nt :: DB -> AppM a -> Handler a
nt s x = runReaderT x s

app :: DB -> Application
app s = serve userAPI $ hoistServer userAPI (nt s) server

main :: IO ()
main = do
  putStrLn "running..."
  initialEnvs <- newTVarIO [Environment (Just "0") "staging"]
  intialIdx <- newTVarIO 0
  initialBookings <- newTVarIO []
  initialBookingIdx <- newTVarIO 0
  run 8081 $ app $ DB initialEnvs intialIdx initialBookings initialBookingIdx

--run 8081 app1

mass = 11

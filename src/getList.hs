{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Relude hiding (head)

import Data.Aeson
import qualified Data.Aeson as JSON
import Servant
import Data.List hiding (elem)

import Network.Wai.Middleware.Cors 
import Network.Wai.Handler.Warp
import qualified Network.HTTP.Types as HTTP

type Task = String
type Tasks = [Task]

data Todo = Todo {
  task :: Task
  ,completed :: Bool
  ,todoId :: Int
                 }
  deriving (Eq, Show, Generic)

type AppM = ReaderT DB Handler

instance ToJSON Todo
instance FromJSON Todo

data DB = DB
  { todos :: TVar [Todo]
  ,lastTodoId :: TVar Int
--    bookings :: TVar [Booking],
--    lastBookingIdx :: TVar Int
  }

getTasks ::  AppM [Todo]
getTasks = do
  DB {todos = p} <- ask
  liftIO $ readTVarIO p
  -- return $ allTasks

createTask :: Todo -> AppM Todo
createTask t = do
  DB {todos = p} <- ask
  liftIO $ atomically $ modifyTVar'  p (\xs -> t:xs)
    -- $ readTVar p >>= writeTVar p . (t:)
  return t

type  GetAPI =  "todos" :> Get '[JSON] [Todo]
type  GetOneAPI =  "todos" :>  Capture "todoId" Int :> Get '[JSON] Todo
-- type  CreateAPI =  "todos" :> ReqBody '[JSON] Todo :> Post '[JSON] Todo
type  CreateAPI =  "todos" :> ReqBody '[JSON] Task :> Post '[JSON] [Todo]-- :> Get '[JSON] [Todo]
-- type GetFirstAPI = "tasks" :> Capture "
type DeleteAPI =  "todos" :> ReqBody '[JSON] Task :> Delete '[JSON] () -- delete
type UpdateTaskAPI =  "todos" :> Capture "todoId" Int :> ReqBody '[JSON] Task :> Put '[JSON] Todo -- update
type ToggleSwitchAPI =  "todos"  :>  ReqBody '[JSON] Int :> Patch '[JSON] [Todo]
type DeleteAllAPI =  "todos" :>   Delete '[JSON] ()
type DeleteCompletedAPI = "todos" :> QueryParam "completed" Bool :> Delete '[JSON] ()

  -- Put '[JSON] Todo -- switch

deleteCompleted :: Maybe Bool -> AppM ()
deleteCompleted mCompleted = do
  DB {todos = p} <- ask
  -- liftIO $ atomically $ modifyTVar' p (filter (not . isCompleted))
  return ()
  {-
  where
    isCompleted todo = case mCompleted of
      Just True -> completed todo
      Just False -> not (completed todo)
      Nothing -> False
-}

getOne :: Int -> AppM Todo
getOne tId = do
  DB {todos =  p} <- ask
  ps <- liftIO $ readTVarIO p
  let maybeIndex = findIndex (\t -> tId == todoId t) ps
  case maybeIndex of
    Nothing ->  throwError err404
    Just ix -> do
      let point = ps !! ix
      -- let newPoint = point {task = newTask}
      -- let newTodo = replaceAtIndex ix newPoint  ps
      -- liftIO $ atomically $ writeTVar p $ newTodo
      return point

type TodoAPI = GetAPI 
-- :<|> CreateAPI :<|> DeleteAPI :<|> UpdateTaskAPI :<|> ToggleSwitchAPI :<|> DeleteAllAPI :<|> DeleteCompletedAPI :<|> GetOneAPI
  
server :: ServerT TodoAPI AppM
-- server = createTask  :<|> getTasks  
server = getTasks 
-- :<|> createTodo :<|> deleteTodoById  :<|> updateTask :<|> toggleSwitch :<|> deleteAllTasks :<|> deleteCompleted :<|> getOne

-- server =  createTask 
-- deleteTodoById :: Int -> Handler ()
deleteTodoById :: Int -> AppM ()
deleteTodoById deleteId = do
  DB {todos = p} <- ask
  liftIO $ atomically $ modifyTVar' p (filter (\x -> deleteId /= todoId x))
  return ()

deleteAllTasks ::  AppM ()
deleteAllTasks = do
  DB {todos = p} <- ask
  liftIO $ atomically $ modifyTVar' p (const [])
  return ()

-- liftIO $ readTVarIO p
  -- liftIO $ print "rgv"
  --readTVar p >>=  writeTVar p . (const [])--  -- liftIO $ read
  --r

createTodo :: Task -> AppM [Todo]
createTodo newTask  = do
  DB {todos = p,lastTodoId = lId} <- ask
  lastId <- liftIO $ readTVarIO lId
  let newTodo = Todo newTask False (lastId+1)
  liftIO $ atomically $ modifyTVar'  p (\xs -> newTodo:xs)
  liftIO $ atomically $ modifyTVar'  lId (+1)
  allTodos <- liftIO $ readTVarIO p
  return $ allTodos
  
updateTask :: Int -> Task -> AppM Todo
updateTask tId newTask =  do
  DB {todos =  p} <- ask
  ps <- liftIO $ readTVarIO p
  let maybeIndex = findIndex (\t -> tId == todoId t) ps
  case maybeIndex of
    Nothing ->  throwError err404
    Just ix -> do
      let point = ps !! ix
      let newPoint = point {task = newTask}
      let newTodo = replaceAtIndex ix newPoint  ps
      liftIO $ atomically $ writeTVar p $ newTodo
      return newPoint

replaceAtIndex :: Int -> a -> [a] -> [a]
replaceAtIndex 0 _ _ = error "index is zero"
replaceAtIndex _ _ [] = error "empty list"
replaceAtIndex 1 item (_:xs) = item:xs
replaceAtIndex n item xs
  | (n <= length xs) && n /= 2 = a ++ (item : b)
  | (n <= length xs) && n == 2 = head xs: item: drop 2 xs
  | otherwise = error "too much index"
  where
    (a, _ : b) = splitAt n xs

toggleSwitch :: Int -> AppM [Todo]
toggleSwitch tId = do
  DB {todos =  p} <- ask
  ps <- liftIO $ readTVarIO p
  let maybeIndex = findIndex (\t -> tId == todoId t) ps
  case maybeIndex of
    Nothing ->  throwError err404
    Just ix -> do
      let newIx = ix+1
      let point = ps !! ix
      let isDone = completed $ point
      -- let newPoint = point {completed = not $ isDone}
      let newPoint = point {completed = not $ isDone}
      let newTodo = replaceAtIndex newIx newPoint  ps
      liftIO $ atomically $ writeTVar p $ newTodo
      allTodos <- liftIO $ readTVarIO p
      return $ allTodos
 
      -- return newPoint

--  delete id x 
  
      -- let updatedDb = deleteTodoById todoId initialDb
      -- return ()

--the actual one
-- type TodoAPI = CreateAPI :> GetAPI --the actual one
-- type TodoAPI =  CreateAPI --the actual one

userAPI :: Proxy TodoAPI
userAPI = Proxy

nt :: DB -> AppM a -> Handler a
nt s x = runReaderT x s
--mySimpleCors 
app :: DB -> Application
app s = mySimpleCors $ serve userAPI $ hoistServer userAPI (nt s) server
-- simpleCors 
main :: IO ()
main = do
  putStrLn "running..."
  initialTodos <- newTVarIO [Todo "watch porn" False 1]
  initialLastId <- newTVarIO 1
  run 8081 $ app $ DB  initialTodos initialLastId
  putStrLn "...closed!!"

mySimpleCors = cors $ const $ Just simpleCorsResourcePolicy {
   -- corsRequestHeaders = ["Authorization", "Content-Type"]
  -- corsOrigins = Just (["http://localhost:8000"],  True)
  corsMethods = ["OPTIONS", "GET", "PATCH", "PUT", "POST"]
  ,corsRequestHeaders =  ["Content-Type"]
    } -- :: [HTTP.HeaderName])
-- "Authorization", 

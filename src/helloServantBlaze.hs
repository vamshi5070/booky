{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

import Network.Wai.Handler.Warp (run)
import Servant
import Servant.HTML.Blaze
import qualified Text.Blaze.Html5 as H 
import qualified Text.Blaze.Html5.Attributes as A

type API = "hello" :> Get '[HTML] H.Html

server :: Server API
server = return $ H.docTypeHtml $ do
    H.head $ do
        H.title "Hello, World!"
        H.style ".my-class { color: blue; }"
    H.body $ do
        H.h1 "Hello, World!"
        H.p "This is my first servant-blaze application."
        H.p H.! A.class_ "my-class" $ "This paragraph has blue text."


api :: Proxy API
api = Proxy

app :: Application
app = serve api server

main :: IO ()
main = run 8080 app

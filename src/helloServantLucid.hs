{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

import Lucid
import Lucid.Servant
import Servant.HTML.Lucid
import Network.Wai.Handler.Warp (run)
import Servant

type MyAPI = "hello" :> Capture "name" String :> Get '[HTML] (Html ())

myAPI :: Proxy MyAPI
myAPI = Proxy

server :: Server MyAPI
server = hello

hello :: String -> Handler (Html ())
hello name = return $ do
  h1_ "Hello, "
  toHtml name

app :: Application
app = serve myAPI server

main :: IO ()
main = run 8080 app

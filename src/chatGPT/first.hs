--import Data.UUID (UUID)
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module TodoAPI where

import Data.Aeson
import Data.Maybe
import Data.List (find, delete)
import qualified Data.UUID as UUID
import Data.UUID (UUID)
import Data.UUID.V4 (nextRandom)
import GHC.Generics
import Network.Wai.Handler.Warp (run)
import Servant
import Relude

-- Define the Todo type
data Todo = Todo
  { todoId :: UUID,
    todoTitle :: String,
    todoCompleted :: Bool
  }
  deriving (Generic)

instance ToJSON Todo
instance FromJSON Todo

-- Define the API
type TodoAPI = "todos" :> Get '[JSON] [Todo] -- get
          :<|> "todos" :> ReqBody '[JSON] Todo :> Post '[JSON] Todo -- add
          :<|> "todos" :> Capture "id" UUID :> Get '[JSON] Todo -- getOne
          :<|> "todos" :> Capture "id" UUID :> ReqBody '[JSON] Todo :> Put '[JSON] Todo -- update
          :<|> "todos" :> Capture "id" UUID :> Delete '[JSON] () -- delete

-- Define a simple in-memory database
type Database = [Todo]

initialDb :: Database
initialDb = 
  [ Todo (fromJust . UUID.fromString $ "f47ac10b-58cc-4372-a567-0e02b2c3d479") "Buy groceries" False,
    Todo (fromJust . UUID.fromString $ "4a7794fe-667c-4d85-9a42-dca416f76a97") "Do laundry" False
  ]
{-
-}

-- Define the server implementation
server :: Server TodoAPI
server = getTodos :<|> addTodo :<|> getTodo :<|> updateTodo :<|> deleteTodo
  where
    getTodos :: Handler [Todo]
    getTodos = return initialDb

    addTodo :: Todo -> Handler Todo
    addTodo todo = do
      newId <- liftIO $ nextRandom
      _ <- liftIO $ print $ newId
      let newTodo = todo {todoId = newId}
      return newTodo

    getTodo :: UUID -> Handler Todo
    getTodo tId = do
      let maybeTodo = find (\t -> tId == todoId t) initialDb
      case maybeTodo of
        Just todo -> return todo
        Nothing -> throwError err404

    updateTodo :: UUID -> Todo -> Handler Todo
    updateTodo tId todo = do
      let maybeIndex = findIndex (\t -> tId == todoId t) initialDb
      case maybeIndex of
        Just index -> do
          let updatedTodo = todo {todoId = tId}
              updatedDb = replaceAtIndex index updatedTodo initialDb
          return updatedTodo
        Nothing -> throwError err404

    deleteTodo :: UUID -> Handler ()
    deleteTodo todoId = do
      let updatedDb = deleteTodoById todoId initialDb
      return ()

-- Helper functions
findIndex :: (a -> Bool) -> [a] -> Maybe Int
findIndex p xs = findIndex' p xs 0
  where
    findIndex' _ [] _ = Nothing
    findIndex' p (x : xs) i
      | p x = Just i
      | otherwise = findIndex' p xs (i + 1)

replaceAtIndex :: Int -> a -> [a] -> [a]
replaceAtIndex n item xs = a ++ (item : b)
  where
    (a, _ : b) = splitAt n xs

deleteTodoById :: UUID -> Database -> Database
deleteTodoById tId db = filter (\t -> tId /= todoId t) db

-- Define the API type
todoAPI :: Proxy TodoAPI
todoAPI = Proxy

-- Define the application
app :: Application
app = serve todoAPI server

-- Run the application
main :: IO ()
main = run 8080 app

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Cors
import qualified Web.Scotty as S
import Servant

type API = "hello" :> Get '[PlainText] String

main :: IO ()
main = S.scotty 8080 $ do
  S.middleware logStdoutDev
  S.middleware simpleCors
  S.get "/scotty" $ do
    S.text "Hello, Scotty!"
  S.liftAndCatchIO $ serve (Proxy :: Proxy API) server

server :: Server API
server = return "Hello, World!"

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

import Servant
import Web.Scotty.Trans
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.Cors 

type HelloWorldAPI = "hello" :> Get '[PlainText] String

helloWorldAPI :: Proxy HelloWorldAPI
helloWorldAPI = Proxy

-- server 

-- server :: Monad m => ServerT HelloWorldAPI (ScottyError String m)
-- server :: ScottyError e => ServerT HelloWorldAPI (ScottyT e IO)
server :: Server HelloWorldAPI 
server = return "Hello, World!\n"

app :: Application
app = simpleCors $ serve helloWorldAPI server

-- app :: ScottyError  e => ScottyT e IO ()
-- app = hoistServer helloWorldAPI (NT runActionT) server

-- serve userAPI $ 
-- scottyApp :: ScottyM ()
-- scottyApp = do
  -- get "/hello" $ do
    -- liftAndCatchIO $ runHandler $ serve helloWorldAPI server

main :: IO ()
main = do
  putStrLn "running"
  -- middleware simpleCors
  run 8080 $ app

{-# LANGUAGE OverloadedStrings, TemplateHaskell, QuasiQuotes, TypeFamilies, MultiParamTypeClasses #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
import Database.Persist.Postgresql
import Database.Persist.TH
import Control.Monad.IO.Class (liftIO)
import Control.Monad
-- import Data.Text (Text)
-- import Data.Text (Text)
import qualified Data.Text as Text

import Control.Monad.Logger (runStdoutLoggingT)


share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Employees
  name Text.Text
  email Text.Text
  deriving Show
|]

hello :: IO ()
hello = do
  -- Create a database connection pool
  pool <- runStdoutLoggingT $ createPostgresqlPool "postgresql://bharat:jodo@localhost/porn" 10

  -- Run a database action inside a connection from the pool
  xs <- runSqlPool (selectList [] []) pool
  forM_ xs $ \(Entity _ (Employees name email)) ->
    putStrLn $ Text.unpack name ++ " is " ++ Text.unpack email

main :: IO ()
main = hello

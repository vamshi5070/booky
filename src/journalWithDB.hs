{-# LANGUAGE OverloadedStrings, TemplateHaskell, QuasiQuotes, TypeFamilies, MultiParamTypeClasses #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}

import Database.Persist.Postgresql
import Database.Persist.TH
import Control.Monad.IO.Class (liftIO)
import Control.Monad 
import Data.Aeson hiding (Key)
import Network.Wai.Middleware.Cors 

import Servant
import qualified Data.Aeson as JSON
import GHC.Generics (Generic)
import qualified Data.Text as Text
import Network.Wai.Handler.Warp (run)

import Control.Monad.Logger (runStdoutLoggingT)

import Data.Pool (Pool)
--import Database.Persist.Postgresql (SqlBackend)
import Relude hiding (head,put,get)

import Data.Int

type Heading = Text.Text
type Content = Text.Text

newtype AppConfig = AppConfig
    { dbConn :: Pool SqlBackend
    }

type AppM m = ReaderT AppConfig m
type App = AppM Handler

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Notes
  heading Heading
  content Content 
  deriving Eq Show Generic
|]

instance ToJSON Notes
--instance FromJSON Notes
instance FromJSON Notes where
    parseJSON = withObject "Notes" $ \v -> Notes
        <$> v .: "heading"
        <*> v .: "content"

instance ToJSON NotesWithId
instance FromJSON NotesWithId

data NotesWithId = NotesWithId
  { noteId :: Int64
  , headingWithId :: Heading
  , contentWithId :: Content
  }
  deriving (Eq, Show, Generic)

withId :: MonadIO m => Int64 -> Notes -> AppM m NotesWithId
withId i notes = do
  pure $
    NotesWithId
      { noteId = i
      ,headingWithId  = notesHeading notes
      , contentWithId = notesContent notes
      }

withStatusMaybe :: MonadIO m => Int64 -> Maybe Notes -> AppM m (Maybe NotesWithId)
withStatusMaybe _ Nothing = pure Nothing
withStatusMaybe i (Just e) = Just <$> withId i e

listNotes :: (MonadIO m) => AppM m [NotesWithId]
listNotes = getNotes >>= mapM entityWithId
  where
    entityWithId (Entity k e) = withId (fromSqlKey k) e

getNotes :: MonadIO m => AppM m [Entity Notes]
getNotes = do
  pool <- asks dbConn
  liftIO $ runSqlPool (selectList [] []) pool

getNote :: MonadIO m => Key Notes -> AppM m (Maybe Notes)
getNote id = do
  pool <- asks dbConn
  liftIO $ runSqlPool (get id) pool

insertNotes :: MonadIO m => Notes -> AppM m (Key Notes)
insertNotes e = do
  pool <- asks dbConn
  liftIO $ runSqlPool (insert e) pool

createNotes :: (MonadIO m) => Notes -> AppM m (Maybe NotesWithId)
createNotes newNotes = do
  id <- insertNotes  newNotes
  env <- getNote id
  withStatusMaybe (fromSqlKey id) env

createAndListNotes  :: (MonadIO m) => Notes -> AppM m [NotesWithId]
createAndListNotes newNotes = do
  maybeNotes <- createNotes newNotes 
  case maybeNotes of
    Nothing -> error "process stopped,due to no entry"
    (Just _) -> listNotes

server :: ServerT TodoAPI App
server = listNotes :<|>  createAndListNotes  

type GetAPI =  "notesHeadings" :> Get '[JSON] [NotesWithId]

type  CreateAPI =  "newNotes" :> ReqBody '[JSON] Notes :> PostCreated '[JSON] [NotesWithId]

type TodoAPI = GetAPI :<|> CreateAPI -- :<|> DeleteAPI :<|> ToggleCompletedAPI :<|> EditTaskAPI



userAPI :: Proxy TodoAPI
userAPI = Proxy

nt :: AppConfig -> App a -> Handler a
nt s x = runReaderT x s

app :: AppConfig -> Application
app s = mySimpleCors $ serve userAPI $ hoistServer userAPI (nt s) server

{-
  :<|> createTodo :<|> deleteTodoById  :<|> updateTask :<|> toggleSwitch :<|> deleteAllTasks :<|> deleteCompleted :<|> getOne

  :<|> CreateAPI :<|> DeleteAPI :<|> UpdateTaskAPI :<|> ToggleSwitchAPI :<|> DeleteAllAPI :<|> DeleteCompletedAPI :<|> GetOneAPI
-}

mySimpleCors = cors $ const $ Just simpleCorsResourcePolicy {
   -- corsRequestHeaders = ["Authorization", "Content-Type"]
  -- corsOrigins = Just (["http://localhost:8000"],  True)
  corsMethods = ["DELETE", "OPTIONS", "GET", "PATCH", "PUT", "POST"]
  ,corsRequestHeaders =  ["Content-Type"]
    } -- :: [HTTP.HeaderName])
-- "Authorization", 

main :: IO ()
main = do
  putStrLn "running...."
  conn <- runStdoutLoggingT $ createPostgresqlPool "postgresql://vamshi:congress@localhost/journal" 1
  run 8083 $ app AppConfig {dbConn = conn}
  putStrLn "closed..."


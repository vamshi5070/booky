{-# LANGUAGE OverloadedStrings, TemplateHaskell, QuasiQuotes, TypeFamilies, MultiParamTypeClasses #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}

import Database.Persist.Postgresql
import Database.Persist.TH
import Control.Monad.IO.Class (liftIO)
import Control.Monad 
import Data.Aeson hiding (Key)
import Network.Wai.Middleware.Cors 

import Servant
import qualified Data.Aeson as JSON
import GHC.Generics (Generic)
import qualified Data.Text as Text
import Network.Wai.Handler.Warp (run)

import Control.Monad.Logger (runStdoutLoggingT)

import Data.Pool (Pool)
import Database.Persist.Postgresql (SqlBackend)
import Relude hiding (head,put,get)

import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import Servant.HTML.Blaze (HTML)

type Task = Text.Text

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Todo
  task Task
  completed Bool
  deriving Eq Show Generic
|]
instance ToJSON Todo
instance FromJSON Todo

instance ToJSON TodoWithId
instance FromJSON TodoWithId

getTasks = undefined

data TodoWithId = TodoWithId
  { todoId :: Int64
  , taskWithId :: Task
  , completedWithId :: Bool 
  }
  deriving (Eq, Show, Generic)

withId :: MonadIO m => Int64 -> Todo -> AppM m TodoWithId
withId i todo = do
  -- now <- liftIO getCurrentTime
  -- bookings <- BookingDB.getActiveBookingsForTodo (toSqlKey i) now
  pure $
    TodoWithId
      { todoId = i
      ,taskWithId  = todoTask todo
      , completedWithId = todoCompleted todo
      }
      
withStatusMaybe :: MonadIO m => Int64 -> Maybe Todo -> AppM m (Maybe TodoWithId)
withStatusMaybe _ Nothing = pure Nothing
withStatusMaybe i (Just e) = Just <$> withId i e
      
listTodos :: (MonadIO m) => AppM m [TodoWithId]
listTodos = getTodos >>= mapM entityWithId
  where
    entityWithId (Entity k e) = withId (fromSqlKey k) e

createTodo :: (MonadIO m) => Todo -> AppM m (Maybe TodoWithId)
createTodo newTodo = do
  id <- insertTodo  newTodo
  env <- getTodo id
  withStatusMaybe (fromSqlKey id) env

createAndListTodo   :: (MonadIO m) => Todo -> AppM m [TodoWithId]
createAndListTodo newTodo = do
  maybeTodo <- createTodo newTodo 
  case maybeTodo of
    Nothing -> error "process stopped,due to no entry"
    (Just foundTodo) -> listTodos

createAndListTodoWithTask   :: (MonadIO m) => Task -> AppM m [TodoWithId]
createAndListTodoWithTask newTask = do
  let newTodo = Todo {todoTask=newTask,todoCompleted=False}
  createAndListTodo newTodo
  
type AppM m = ReaderT AppConfig m
type App = AppM Handler

getTodo :: MonadIO m => Key Todo -> AppM m (Maybe Todo)
getTodo id = do
  pool <- asks dbConn
  liftIO $ runSqlPool (get id) pool

getTodos :: MonadIO m => AppM m [Entity Todo]
getTodos = do
  pool <- asks dbConn
  liftIO $ runSqlPool (selectList [] []) pool
  
insertTodo :: MonadIO m => Todo -> AppM m (Key Todo)
insertTodo e = do
  pool <- asks dbConn
  liftIO $ runSqlPool (insert e) pool

type  GetAPI =  "todos" :> Get '[JSON] [TodoWithId]
type  GetAPIWithHTML =  "todos" :> Get '[HTML] H.Html

type  CreateAPISingle =  "todos" :> ReqBody '[JSON] Todo :> PostCreated '[JSON] (Maybe TodoWithId)
-- type  CreateAPI =  "todos" :> ReqBody '[JSON] Todo :> PostCreated '[JSON] [TodoWithId]
type  CreateAPI =  "todos" :> ReqBody '[JSON] Task :> PostCreated '[JSON] [TodoWithId]
type  CreateAPIHTML =  "todos" :> ReqBody '[JSON] Task :> PostCreated '[JSON] [TodoWithId]
  -- :> Get '[JSON] [TodoWithId]

type TodoAPI = GetAPIWithHTML

--GetAPI :<|> CreateAPI

getTodosPage :: (MonadIO m) => AppM m H.Html-- [TodoWithId]
getTodosPage = do
  todos <- listTodos
  return $ H.docTypeHtml $ do
    H.head $ do
      H.title "Todos"
    H.body $ do
      H.h1 "Items"
      H.form H.! A.method "post" $ do
        H.label H.! A.for "name" $ "Name:"
        H.input H.! A.type_ "text" H.! A.name "name" H.! A.id "name"
--        H.label ! A.for "price" $ "Price:"
--        H.input ! A.type_ "number" ! A.name "price" ! A.id "price"
        H.input H.! A.type_ "submit" H.! A.value "Add"

{-      
--        H.label H.! A.for "search" $ "Search:"
        H.input H.! A.type_ "text" H.! A.name "search" H.! A.id "search"
        H.input H.! A.type_ "submit" H.! A.value "Go"
      H.ul $ do
        mapM_ renderTodo todos
 -}       

server :: ServerT TodoAPI App
-- server = createTask  :<|> getTasks  
server =  getTodosPage
        
renderTodo :: TodoWithId -> H.Html
renderTodo todo = 
  H.li $ do
    H.toHtml $ taskWithId todo
--listTodos :<|> createAndListTodoWithTask

newtype AppConfig = AppConfig
    { dbConn :: Pool SqlBackend
    }

userAPI :: Proxy TodoAPI
userAPI = Proxy

nt :: AppConfig -> App a -> Handler a
nt s x = runReaderT x s

app :: AppConfig -> Application
app s = simpleCors $ serve userAPI $ hoistServer userAPI (nt s) server


--mySimpleCors 
{-

  :<|> createTodo :<|> deleteTodoById  :<|> updateTask :<|> toggleSwitch :<|> deleteAllTasks :<|> deleteCompleted :<|> getOne

  :<|> CreateAPI :<|> DeleteAPI :<|> UpdateTaskAPI :<|> ToggleSwitchAPI :<|> DeleteAllAPI :<|> DeleteCompletedAPI :<|> GetOneAPI
-}
mySimpleCors = cors $ const $ Just simpleCorsResourcePolicy {
   -- corsRequestHeaders = ["Authorization", "Content-Type"]
  -- corsOrigins = Just (["http://localhost:8000"],  True)
  corsMethods = ["OPTIONS", "GET", "PATCH", "PUT", "POST"]
  ,corsRequestHeaders =  ["Content-Type"]
    } -- :: [HTTP.HeaderName])
-- "Authorization", 

main :: IO ()
main = do
  putStrLn "running...."
  conn <- runStdoutLoggingT $ createPostgresqlPool "postgresql://vamshi:congress@localhost/todosdb" 1
  run 8081 $ app AppConfig {dbConn = conn}
  putStrLn "closed..."


  -- Create a database connection pool
  -- pool <- runStdoutLoggingT $ createPostgresqlPool "postgresql://vamshi:congress@localhost/todosdb" 10

  -- Run a database action inside a connection from the pool
  {-
  xs <- runSqlPool (selectList [] []) pool
  forM_ xs $ \(Entity _ (Todo name completed)) -> do
    let status = if completed then "" else "not"
    putStrLn $ Text.unpack name ++ " is " ++  status ++ " completed."

postTodo :: IO ()
postTodo = do
  -- Create a database connection pool
  -- pool <- runStdoutLoggingT $ createPostgresqlPool "postgresql://vamshi:congress@localhost/todosdb" 10
  -- Define a new employee
  let newTodo = Todo "have sex"  False--"johndoe@example.com"

  -- Insert the new employee into the database
  insertedTodoKey <- runSqlPool (insert newTodo) pool

  -- Fetch the inserted employee from the database
  insertedTodo <- runSqlPool (get insertedTodoKey) pool
  
  -- Print the inserted employee's name and email
  case insertedTodo of
    Just (Todo name completed) -> do
      let status = if completed then "" else "not"
      putStrLn $ Text.unpack name ++ " is " ++  status ++ " completed."
      -- putStrLn $ Text.unpack name ++ " is " ++ Text.unpack email
    Nothing ->
      putStrLn "Failed to fetch the inserted employee"
 -}
  
-- import Data.Text (Text)
-- import Data.Text (Text)

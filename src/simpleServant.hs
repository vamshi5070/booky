{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}

import Relude

import Data.Aeson
import qualified Data.Aeson as JSON
import Servant
import Data.List hiding (elem)

import Network.Wai.Handler.Warp


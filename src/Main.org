#+PROPERTY: header-args  :tangle Main.hs :align left
#+OPTIONS: toc:2 :latex-class article :latex-header \usepackage{graphicx}
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css" />
#+STARTUP: hideblocks

* Pragmas
#+begin_src haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
#+end_src
* Module declaration
#+begin_src haskell
module Main where
#+end_src
* Imports
#+begin_src haskell
--import Servant.Client
--import Servant.Server
import Data.Aeson
import qualified Data.Aeson as JSON
--import Network.Wai

import Data.Time
import Network.Wai.Handler.Warp
import Relude
import Servant
#+end_src
* Booking type
#+begin_src haskell
data Booking = Booking
  { bookingID :: Maybe ID,
    bookingEnvID :: Maybe ID,
    bookingFrom :: UTCTime
  }
  deriving (Eq, Show, Generic)
#+end_src
* Database
#+begin_src haskell
data DB = DB
  { environments :: TVar [Environment],
    lastEnvIdx :: TVar Int,
    bookings :: TVar [Booking],
    lastBookingIdx :: TVar Int
  }
#+end_src

* ID
#+begin_src haskell
type ID = String
#+end_src
* Environment type
#+begin_src haskell
data Environment = Environment
  { envID :: Maybe ID,
    name :: String
  }
  deriving (Eq, Show, Generic)

#+end_src
* Null Environment
#+begin_src haskell
nullEnvironment :: Environment
nullEnvironment = Environment {envID = Nothing, name = ""}
#+end_src
* EnvironmentAPI
core
display
Create new
Show individual through respective search
#+begin_src haskell
type EnvironmentAPI = FirstAPI
 :<|> SecondAPI
 :<|> ThirdAPI
 :<|> FourthAPI
 :<|> FiveAPI
type  FirstAPI =  "environments" :> Get '[JSON] [Environment]
type SecondAPI = "environments" :> ReqBody '[JSON] Environment :> PostCreated '[JSON] Environment
type ThirdAPI = "environments" :> Capture "id" String :> Get '[JSON] Environment
type FourthAPI = "bookings" :> Get '[JSON] [Booking]
type FiveAPI = "environments" :> Capture "env_id" String :> "bookings" :> ReqBody '[JSON] Booking :> PostCreated '[JSON] Booking
-- :<|>
#+end_src
* App Monad
#+begin_src haskell
type AppM = ReaderT DB Handler
#+end_src
* From and to json
#+begin_src haskell
instance ToJSON Environment

instance FromJSON Environment

instance ToJSON Booking

instance FromJSON Booking where
  parseJSON (Object v) =
    Booking Nothing Nothing <$> v .: "booking_from"
  --       <*> v .: "envID"
  parseJSON _ = mzero


#+end_src

* Increment
#+begin_src haskell
inc :: Int -> Int
inc = (+ 1)
#+end_src
* My environment
#+begin_src haskell



createEnvironment :: Environment -> AppM Environment
createEnvironment e = do
  DB {environments = p, lastEnvIdx = q} <- ask
  i <- readTVarIO q
  let e' = e {envID = Just $ show i}
  liftIO $ atomically $ readTVar p >>= writeTVar p . (e' :)
  liftIO $ atomically $ readTVar q >>= writeTVar q . inc --(e {envID = i} : )
  return e

listEnvironments :: AppM [Environment]
listEnvironments = do
  DB {environments = p} <- ask
  liftIO $ readTVarIO p

createBooking :: String -> Booking -> AppM Booking
createBooking envId b = do
  DB {environments = o, bookings = p, lastBookingIdx = q} <- ask
  envs <- readTVarIO o
  case find (\x -> envID x == Just envId) envs of
    Nothing -> throwError err404 {errBody = JSON.encode $ "Environment with ID " ++ envId ++ " not found."}
    Just _ -> do
      i <- readTVarIO q
      let b' = b {bookingID = Just $ show i, bookingEnvID = Just envId}
      liftIO $ atomically $ readTVar p >>= writeTVar p . (b' :)
      liftIO $ atomically $ readTVar q >>= writeTVar q . inc --(e {envID = i} : )
      return b'

getEnvironment :: String -> AppM Environment
getEnvironment i = do
  DB {environments = p} <- ask
  environments <- readTVarIO p
  case (find (\x -> envID x == Just i) environments) of
    Just e -> return e
    Nothing -> throwError err404 {errBody = JSON.encode $ "Environment with ID " <> i <> " not found."}

listBookings :: AppM [Booking]
listBookings = do
  DB {bookings = p} <- ask
  liftIO $ readTVarIO p

server :: ServerT EnvironmentAPI AppM
server = listEnvironments :<|> createEnvironment :<|> getEnvironment :<|> listBookings :<|> createBooking

--server1 :: Server UserAPI1
--server1 = return users1 :<|> singleUser

userAPI :: Proxy EnvironmentAPI
userAPI = Proxy

--singleUser = undefined
-- return $ fromMaybe (User "" 0 "") (find (\x -> name x == name') users1)

--app1 :: Application
--app1 = serve userAPI server

nt :: DB -> AppM a -> Handler a
nt s x = runReaderT x s

app :: DB -> Application
app s = serve userAPI $ hoistServer userAPI (nt s) server

main :: IO ()
main = do
  putStrLn "running..."
  initialEnvs <- newTVarIO []
  intialIdx <- newTVarIO 0
  initialBookings <- newTVarIO []
  initialBookingIdx <- newTVarIO 0
  run 8081 $ app $ DB initialEnvs intialIdx initialBookings initialBookingIdx

--run 8081 app1

#+end_src

* Example
#+begin_src haskell
mass = 11
#+end_src



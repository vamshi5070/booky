-- {-# LANGUAGE NoImplicitPrelude #-}
-- {-# LANGUAGE OverloadedStrings #-}

-- import Relude hiding (head)
import Data.Text
import Data.Maybe
import Text.Read
import System.IO

--import Relude.Extra

main :: IO ()
main = do
  putStrLn "Happy journalling"
  putStrLn ".\n.\n.\n.\n.\n.\n.\n.\n.\n.\n"
  putStrLn "How many journals:- "
  putStr "Enter no of enteries to be listed:- "
  hFlush stdout
  num <-  (read <$> getLine) :: IO Int
  loop num

loop :: Int -> IO ()
loop num  
  | num <= 0 = return ()
  | otherwise = do
                  putStrLn "Enter first entry heading"
                  heading <- getLine
                  putStrLn "Enter first entry content"
                  content <- getLine
                  putStr  "Your first heading: " 
                  putStrLn  heading
                  putStr  "Your first content: " 
                  putStrLn  content
                  loop (num - 1)

{-

  let parsedInt = readMaybe num :: Maybe Integer
-}
--getInt :: IO Int
--getInt = readMaybe getLine 

-- readMaybe <$> fromJust <$> 

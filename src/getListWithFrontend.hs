{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Relude hiding (head)
import Data.List as L
import Data.Aeson
import qualified Data.Aeson as JSON
import Servant
import Data.List hiding (elem)

import Network.Wai.Middleware.Cors 
import Network.Wai.Handler.Warp
import qualified Network.HTTP.Types as HTTP

-- for frontend
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import Servant.HTML.Blaze (HTML)
--import Text.Blaze.Html5 as H

type Task = String
type Tasks = [Task]

data Todo = Todo {
  task :: Task
  ,completed :: Bool
  ,todoId :: Int
                 }
  deriving (Eq, Show, Generic)

type AppM = ReaderT DB Handler

instance ToJSON Todo
instance FromJSON Todo

data DB = DB
  { todos :: TVar [Todo]
  ,lastTodoId :: TVar Int
--    bookings :: TVar [Booking],
--    lastBookingIdx :: TVar Int
  }

getTasks ::  AppM [Todo]
getTasks = do
  DB {todos = p} <- ask
  liftIO $ readTVarIO p
  -- return $ allTasks

type  GetAPI =  "todos" :> Get '[HTML] H.Html
  -- Put '[JSON] Todo -- switch

type TodoAPI = GetAPI 
-- :<|> CreateAPI :<|> DeleteAPI :<|> UpdateTaskAPI :<|> ToggleSwitchAPI :<|> DeleteAllAPI :<|> DeleteCompletedAPI :<|> GetOneAPI

getTasksHtml :: AppM H.Html
getTasksHtml =  do
  DB {todos = p} <- ask
  todos <- liftIO $ readTVarIO p
  return $ H.docTypeHtml $ do
    H.head $ do
      H.title "Todos"
    H.body $ do
      H.ul $ do
        mapM_ renderTodo todos

renderTodo :: Todo -> H.Html
renderTodo todo = 
  H.li $ do
    H.toHtml $ task todo
  
server :: ServerT TodoAPI AppM
-- server = createTask  :<|> getTasks  
server = getTasksHtml

userAPI :: Proxy TodoAPI
userAPI = Proxy

nt :: DB -> AppM a -> Handler a
nt s x = runReaderT x s

app :: DB -> Application
app s = mySimpleCors $ serve userAPI $ hoistServer userAPI  (nt s) server

-- simpleCors 
main :: IO ()
main = do
  putStrLn "running..."
  initialTodos <- newTVarIO [Todo "watch porn" False 1]
  initialLastId <- newTVarIO 1
  run 8081 $ app $ DB  initialTodos initialLastId
  putStrLn "...closed!!"

mySimpleCors = cors $ const $ Just simpleCorsResourcePolicy {
   -- corsRequestHeaders = ["Authorization", "Content-Type"]
  -- corsOrigins = Just (["http://localhost:8000"],  True)
  corsMethods = ["OPTIONS", "GET", "PATCH", "PUT", "POST"]
  ,corsRequestHeaders =  ["Content-Type"]
    } -- :: [HTTP.HeaderName])
-- "Authorization", 

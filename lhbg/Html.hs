module Html
 ( Html
  , Title
  , Structure
  , html_
  , p_
  , (<>)
  , h1_
  , h_
  , ul_
  , ol_
  , code_
  , append_
  , render
  , compose
 )
  where

import Html.Internal

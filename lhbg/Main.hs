import Markup
import Convert
import Html

import System.Directory (doesFileExist)
import System.Environment (getArgs)
import System.IO (getContents, readFile, writeFile)

process :: Title -> String -> String
process title content = render $ convert  title (parse content)

confirm :: IO Bool
confirm =
  putStrLn "Are you sure? (y/n)" *>
    getLine >>= \answer ->
      case answer of
        "y" -> pure True
        "n" -> pure False
        _ -> putStrLn "Invalid response. use y or n" *>
          confirm

whenIO :: IO Bool -> IO () -> IO ()
whenIO cond action =
  cond >>= \result ->
    if result
      then action
      else pure ()

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] -> do
      content <- getContents  
      putStrLn $ process "Empty title" content
    [input,output] -> do
      content <- readFile input 
      exists <- doesFileExist output
      let writeResult = writeFile output (process input content)
          in
        if exists then
         whenIO confirm writeResult
         else
         writeResult
    _ -> putStrLn "Usage: runghc Main.hs [-- <input-file> <output-file>]"
          
    

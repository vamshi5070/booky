-- Html/Internal.hs

module Html.Internal where

import GHC.Natural

main = putStrLn $ render $ html_ "My page title" (body_ "My page content")
-- myHtml

-- myHtml = wrapHtml "Hello, world?"

-- wrapHtml =
  --html_ . getStructureString .  body_
 -- "<html><body> "<> content <> "</body></html>"
-- head_  = Structure . el "head"

-- title_  = el "title"

html_ :: Title -> Structure -> Html
html_ title (Structure content) = Html
  (el "html"
   (el "head" (el "title" (escape title)))
   <> (el "body"  content)
  )

body_ = Structure . el "body" . escape

p_ :: String -> Structure 
p_ = Structure . el "p" . escape

code_ :: String -> Structure 
code_ = Structure . el "pre" . escape

h1_ :: String -> Structure
h1_ = Structure . el "h1" . escape

h_ :: Natural -> String -> Structure
h_ n = Structure . el ("h" <> show n) . escape

-- makeHtml title content = html_  $ getStructureString $ (head_ `compose` title_ $ title) `append_` (body_ content)


append_ :: Structure -> Structure -> Structure
append_ (Structure str1) (Structure str2) = Structure $ str1 <> str2

render :: Html -> String
render html =
  case html of
    (Html str) -> str

el :: String -> String -> String
el part content = first <> content <> last
  where
    first = "<" <> part <> ">"
    last = "<" <> part <> "/>"

newtype Html = Html String
  deriving (Eq,Show)

newtype Structure = Structure String
  deriving (Eq,Show)

type Title = String

getStructureString :: Structure -> String
getStructureString (Structure str) = str

compose :: (String -> Structure) -> (String -> Structure) ->  (String -> Structure)
compose s1 s2 = s1 .  getStructureString .  s2

escape :: String -> String
escape = let
  escapeChar c =
    case c of
      '<' -> "&lt;"
      '>' -> "&gt;"
      '&' -> "&amp;"
      '"' -> "&quot;"
      '\'' -> "&#39;"
      _ -> [c]
      in
    concatMap escapeChar

ul_ :: [Structure] -> Structure
ul_ ss = Structure $ el "ul" $ concat $ map (\x ->  el "li" (getStructureString x)) ss

ol_ :: [Structure] -> Structure
ol_  = Structure . el "ol" . concat . map ( el "li" . getStructureString) 

empty_ :: Structure
empty_ = Structure ""

concatStructure :: [Structure] -> Structure
concatStructure list =
  case list of
    [] -> empty_
    x : xs -> x <> concatStructure xs

instance Semigroup Structure where
  (<>) = append_

instance Monoid Structure where
  mempty = empty_
  mconcat = concatStructure

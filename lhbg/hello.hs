import Html
import Markup
import Convert

myhtml :: Html
myhtml =
  html_
  "My title"
  (append_
    (h1_ "Heading")
    (append_
     (p_ "Paragraph #1")
     (p_ "Paragraph #1")
    )
  )

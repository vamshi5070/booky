{
  description = "booky";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };



  outputs = { self, nixpkgs, flake-utils, }:
    let utils = flake-utils.lib;
    in utils.eachDefaultSystem (system:
      let
        compilerVersion = "ghc924";
        pkgs = nixpkgs.legacyPackages.${system};
        hsPkgs = pkgs.haskell.packages.${compilerVersion}.override {
          overrides = hfinal: hprev: {
            booky = hfinal.callCabal2nix "booky" ./. { };
          };
        };
      in rec {
        packages = utils.flattenTree { booky = hsPkgs.booky; };

        devShell = hsPkgs.shellFor {
          withHoogle = true;
          packages = p: [ p.booky ];
          buildInputs = [
            # pkgs.cabal2nix
            pkgs.cabal-install
            #          pkgs.postgresql
            pkgs.hlint
            pkgs.ormolu
            pkgs.haskell.compiler."${compilerVersion}"
            hsPkgs.ghcid
            pkgs.jq
            # pkgs.qemu
            # pkgs.purescript
            # hsPkgs.haskell-language-server
            # hsPkgs.alex
            # hsPkgs.happy
            # hsPkgs.hspec-discover
          ];
          # ++ (builtins.attrValues (import ./scripts.nix {s = pkgs.writeShellScriptBin;}));
        };

        defaultPackage = packages.booky;
      });
}

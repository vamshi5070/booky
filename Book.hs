{-#Language QuasiQuotes#-}

module Book where

import Prelude ()
import Relude

--import Control.Monad

import qualified Data.Char as Char
import Data.Maybe

import qualified System.Directory as Dir
import System.FilePath ((</>))

import Control.Monad.Trans.Resource (ReleaseKey, ResourceT, allocate, runResourceT)
import qualified Control.Exception.Safe as Ex

import qualified System.IO as IO

import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Encoding as T

--import qualified Data.Text.Lazy.IO as LT
import qualified Data.Text.Lazy.Encoding as LT
import qualified Data.Text.Lazy as LT

import qualified Data.Text.Lazy.Builder as TB
import qualified Data.Text.Lazy.Builder.Int as TB

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Builder as BSB
--import qualified Data.ByteString.Char8 as B

import Network.Socket (Socket)
import qualified Network.Socket as S
import qualified Network.Socket.ByteString as S

import qualified ASCII as A
import qualified ASCII.Char as A
import ASCII.Decimal (Digit (..))

import Network.Simple.TCP (serve, HostPreference (..))
import qualified Network.Simple.TCP as Net

import qualified Data.Time as Time

import Text.Blaze.Html (Html, toHtml)
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import qualified Text.Blaze.Html5 as HTML

import qualified Data.Aeson as J
import qualified Data.Aeson.Key as J.Key
import qualified Data.Aeson.KeyMap as J.KeyMap
import Data.Aeson (ToJSON (toJSON))
import Data.Aeson ((.=))

import qualified Control.Concurrent.Async as Async
import Control.Concurrent (threadDelay)

import List.Transformer (ListT, runListT)
import qualified List.Transformer as ListT

import qualified Data.Attoparsec.ByteString as P
import Data.Attoparsec.ByteString (Parser, (<?>))

import qualified Data.Map.Strict as Map

getDataDir :: IO FilePath
getDataDir = do
 dir <- Dir.getXdgDirectory Dir.XdgData "sockets-and-pipes"
 Dir.createDirectoryIfMissing True dir
 return dir

writeGreetingFile :: IO ()
writeGreetingFile = do
 dir <- getDataDir
 h <- IO.openFile (dir </> "greeting.txt") WriteMode
 IO.hPutStrLn h "hello"
 IO.hPutStrLn h "world"
 IO.hClose h

fileResource ::
     FilePath -> IOMode -> ResourceT IO (ReleaseKey, Handle)
fileResource fp mode = allocate (IO.openFile fp mode) IO.hClose

writeGreetingSafe :: IO ()
writeGreetingSafe = runResourceT @IO do
  dir <- liftIO getDataDir
  (_releaseKey, h) <- allocate
    (IO.openFile (dir </> "greeting.txt") WriteMode)
    IO.hClose
  liftIO (IO.hPutStrLn h "hello")
  liftIO (IO.hPutStrLn h "world")

-- mass = fileResource undefined
writeGreetingSafeMine :: IO ()
writeGreetingSafeMine = runResourceT @IO do
  dir <- liftIO getDataDir
  let dir' = dir </> "greeting.txt"
  (_releaseKey, h) <- fileResource dir' WriteMode
  liftIO (IO.hPutStrLn h "hello")
  liftIO (IO.hPutStrLn h "world")

howManyHandles :: IO ()
howManyHandles = runResourceT @IO do
  hs <- openManyHandles
  putStrLn ("Opened " <> show (length hs) <> " handles")

openManyHandles :: ResourceT IO [Handle]
openManyHandles = do
  file <-  fileResourceMaybe
  case file of
    Nothing -> return []
    Just x -> do
      res <- openManyHandles
      return $ x:res
        -- dir <- liftIO getDataDir
    -- let dir' = dir </> "greeting.txt"
    -- (_,h) <- fileResource dir' WriteMode
    -- case result of
      -- error _ =
    -- return [h]
    --undefined

fileResourceMaybe :: ResourceT IO (Maybe Handle)
fileResourceMaybe = do
  dir <- liftIO getDataDir
  -- let dir' = dir </> "greeting.txt"
  result <- Ex.tryIO  do
    (_releaseKey, h) <-  fileResource dir WriteMode
    return h
  case result of
    Right x -> return $ Just x
    Left e -> do
      print (displayException e)
      return Nothing

main :: IO ()
main = putStr "main"

printFileContentsUpperCase :: IO ()
printFileContentsUpperCase = runResourceT @IO do
  dir <- liftIO getDataDir
  (_, h) <- fileResource (dir </> "greeting.txt") ReadMode
  liftIO (printCapitalizedText h)

printCapitalizedText :: Handle -> IO ()
printCapitalizedText h = continue
  where
    continue = do
      chunk <- T.hGetChunk h
      case (T.null chunk) of
        True -> return ()
        False -> do
          T.putStr (T.toUpper chunk)
          continue

repeatUntilIO :: IO chunk -> (chunk -> Bool) -> (chunk -> IO x) -> IO ()
repeatUntilIO getChunk isEnd f = continue
  where continue = do
          chunk <- getChunk
          case (isEnd chunk) of
            True -> return ()
            False -> do
              {f chunk
                 >> continue
                 }

printFileContentsUpperCase2 :: IO ()
printFileContentsUpperCase2 = runResourceT @IO do
  dir <- liftIO getDataDir
  (_, h) <- fileResource (dir </> "greeting.txt") ReadMode
  liftIO $ repeatUntilIO (T.hGetChunk h) T.null \chunk ->
    T.putStr (T.toUpper chunk)

--digitsOnly :: Text -> Text
digitsOnly :: Text -> Text
digitsOnly = T.pack . filter Char.isNumber . T.unpack

capitaliseLast :: Text -> Text
capitaliseLast text =  cap <> rest
 where cap = T.init text
       rest = T.pack  [Char.toUpper $ T.last text]

unParen :: Text -> Maybe Text
unParen = fmap T.pack .  (\(x,y,str)-> if x && y then Just str else Nothing) . foldr helper (False,False,[]) . T.unpack
  where
    helper ')' (x,y,xs) = if (not x && not y ) then (x,True,xs) else (x,y,')':xs)
    helper '(' (x,y,xs) = if (y && not (x) )then (True,y,xs) else (x,y,'(':xs)
    helper ch (x,y,xs) = (x,y,ch:xs)

--characterCount :: FilePath -> IO Int
--characterCount :: FilePath -> IO ()
characterCount :: FilePath -> IO ()
characterCount fp = runResourceT @IO do
  dir <- liftIO getDataDir
  (_, h) <- fileResource (dir </> fp) ReadMode
  liftIO $ characterCount' h

--  liftIO $ repeatUntilIO (T.hGetChunk h) T.null \chunk ->
characterCount' :: Handle -> IO ()
characterCount' h = continue 0
  where
    continue cnt = do
      chunk <- T.hGetChunk h
      case (T.null chunk) of
        True -> do
          print cnt
          return ()
        False -> do
--          T.putStrLn (T.toUpper chunk)
          let cnt' = T.length chunk
          continue (cnt + cnt')

--when' :: Bool -> IO () -> IO ()
when' :: Monad m => Bool -> m () -> m ()
when' p f
  | p = f
  | otherwise = return ()

unless' :: Monad m => Bool -> m () -> m ()
--unless' :: Bool -> IO () -> IO ()
unless' p = when' (not p)

repeatUntil
  :: Monad m => m t -> (t -> Bool) -> (t -> m a) -> m ()
repeatUntil getChunk isEnd f = do
  chunk <- getChunk
  unless' (isEnd chunk) do
                         _ <- f chunk
                         repeatUntil getChunk isEnd f

copyGreetingFile :: IO ()
copyGreetingFile = runResourceT @IO do
  dir <- liftIO getDataDir
  (_, h1) <- binaryFileResource (dir </> "greeting.txt") ReadMode
  (_, h2) <- binaryFileResource (dir </> "greeting2.txt") WriteMode
  liftIO $ repeatUntil (BS.hGetSome h1 1024) BS.null \chunk -> BS.hPutStr h2 chunk

binaryFileResource ::
     FilePath -> IOMode -> ResourceT IO (ReleaseKey, Handle)
binaryFileResource fp mode = allocate (IO.openBinaryFile fp mode) IO.hClose

exampleBytes :: [Word8]
exampleBytes = [104, 101, 108, 108, 111]

helloByteString :: IO ()
helloByteString = do
  IO.hSetBinaryMode stdout True
  BS.hPut stdout (BS.pack helloBytes)

helloBytes :: [Word8]
helloBytes = [
 104, 101, 108, 108, 111,
 32,
 119, 111, 114, 108, 100, 33,
 10
 ]

helloUtf8 :: IO ()
helloUtf8 = do
  IO.hSetBinaryMode stdout True
  BS.hPutStr stdout (T.encodeUtf8 (T.pack "hello world!\n"))

greet :: BS.ByteString -> IO ()
greet nameBS = T.putStrLn $ T.pack "Hello, " <> T.decodeUtf16LE nameBS
  -- case T.decodeUtf8' nameBS of
--  Left _ -> putStrLn "Invalid byte string"
--  Right nameText -> T.putStrLn (T.pack  "Hello, " <> nameText)

asciiUpper :: BS.ByteString -> BS.ByteString
asciiUpper = BS.map  helper
  where helper x
          | x > 122 || x < 97 = x
          | otherwise = x - 32

makeFriend :: S.SockAddr -> IO ()
makeFriend address = do
  s <- S.socket S.AF_INET S.Stream S.defaultProtocol
  S.connect s address
  S.sendAll s $ T.encodeUtf8 $
    T.pack "Hello, will you be my friend?"
  repeatUntil (S.recv s 1024) BS.null BS.putStr

makeFriendSafely :: Net.SockAddr -> IO ()
makeFriendSafely address = runResourceT @IO do
  (_,s) <- allocate
    (S.socket S.AF_INET S.Stream S.defaultProtocol)
    S.close

  liftIO do
    S.connect s address
    S.sendAll s $ T.encodeUtf8 $
      T.pack "Hello, will you be my friend?"
    repeatUntil (S.recv s 1024) BS.null BS.putStr
    S.gracefulClose s 1000

findHaskellWebsite :: IO S.AddrInfo
findHaskellWebsite = do
  addrInfos <- S.getAddrInfo
    (Just S.defaultHints { S.addrSocketType = S.Stream})
    (Just "www.haskell.org")
    (Just "http")
  case addrInfos of
    [] -> fail "getAddrInfo returned []"
    x : _ -> return x

resolve :: S.ServiceName -> S.HostName -> IO S.AddrInfo
resolve serviceName hostName = do
  addrInfos <- S.getAddrInfo
    (Just S.defaultHints { S.addrSocketType = S.Stream})
    (Just hostName)
    (Just serviceName)
  case addrInfos of
    [] -> fail "getAddrInfo returned []"
    x : _ -> return x

makeFriendAddrInfo :: S.AddrInfo -> IO ()
makeFriendAddrInfo addressInfo = runResourceT @IO do
  (_, s) <- allocate (S.openSocket addressInfo) S.close
  liftIO do
    S.setSocketOption s S.UserTimeout 1000
    S.connect s (S.addrAddress addressInfo)
    S.sendAll s $ T.encodeUtf8 $
      T.pack "Hello, will you be my friend?"
    repeatUntil (S.recv s 1024) BS.null BS.putStr
    S.gracefulClose s 1000

openAndConnect :: S.AddrInfo -> ResourceT IO (Either SomeException (ReleaseKey, Socket))
openAndConnect addressInfo = Ex.try $ allocate setup S.close
  where
    setup = do
      s <- S.openSocket addressInfo
      S.setSocketOption s S.UserTimeout 1000
      S.connect s (S.addrAddress addressInfo)
      return s

makeFriendAddrInfo' :: S.AddrInfo -> IO ()
makeFriendAddrInfo' addressInfo = runResourceT @IO do
  res <- openAndConnect addressInfo
  case res of
    Left _ -> fail "connection failed"
    Right (_, s) ->
     liftIO do
      S.sendAll s $ T.encodeUtf8 $
       T.pack "Hello, can you be my friend?"
      repeatUntil (S.recv s 1024) BS.null BS.putStr
      S.gracefulClose s 1000

--  (_, s) <-  openAndConnect addressInfo  --allocate (S.openSocket addressInfo) S.close

findHaskellWebsite' :: IO S.AddrInfo
findHaskellWebsite' = do
  addrInfos <- S.getAddrInfo
    (Just S.defaultHints { S.addrSocketType = S.Stream})
    (Just "quux.org")
    (Just "gopher")
  case addrInfos of
    [] -> fail "getAddrInfo returned []"
    x : _ -> return x

--line :: BS.ByteString -> BS.ByteString
--line x = x <> fromString "\r\n"

helloRequestString :: ByteString
helloRequestString =
  line [A.string|GET /hello.txt HTTP/1.1|] <>
  line [A.string|User-Agent: curl/7.16.3|] <>
  line [A.string|Accept-Language: en, mi|] <>
  line [A.string||]

crlf :: [A.Char]
crlf = [A.CarriageReturn, A.LineFeed]

line :: BS.ByteString -> BS.ByteString
line x = x <> A.lift crlf

helloResponseString :: BS.ByteString
helloResponseString =
  line [A.string|HTTP/1.1 200 OK|] <>
  line [A.string|Content-Type: text/plain; charset=us-ascii|] <>
  line [A.string|Content-length: 6|] <>
  line [A.string||] <>
  [A.string|Hello!|]

ourFirstServer :: IO a
ourFirstServer = serve @IO HostAny "8000" \(s, a) -> do
  putStrLn ("New connection from " <> show a)
  Net.send s helloResponseString

repeatUntilNothing :: Monad m =>
      m (Maybe chunk) -> (chunk -> m a) -> m ()
repeatUntilNothing getChunkMaybe f = do
  chunk <- getChunkMaybe
  case chunk of
    Nothing -> return ()
    Just x -> do
      _ <- f x
      repeatUntilNothing getChunkMaybe f

--  unless' (chunk == Nothing) do
--    _ <- f chunk
repeatUntilNothing' :: Monad m =>
      m (Maybe chunk) -> (chunk -> m a) -> m ()
repeatUntilNothing' getChunkMaybe f = repeatUntil getChunkMaybe isNothing (f . fromJust)
  --repeatUntil getChunkMaybe (== Nothing) f

makeFriendAddrInfo'' :: S.AddrInfo -> IO ()
makeFriendAddrInfo'' addressInfo = runResourceT @IO do
  res <- openAndConnect addressInfo
  case res of
    Left _ -> fail "connection failed"
    Right (_, s) ->
     liftIO do
      S.sendAll s $ T.encodeUtf8 $
       T.pack "Hello, can you be my friend?"
      repeatUntil (S.recv s 1024) BS.null BS.putStr
      S.gracefulClose s 1000

haskellRequestString :: BS.ByteString
haskellRequestString =
  line [A.string|GET / HTTP/1.1|] <>
  line [A.string|Host: haskell.org|] <>
  line [A.string|Connection: close|] <>
  line [A.string||]

makeHaskellHttpRequest :: ByteString -> S.AddrInfo -> IO ()
makeHaskellHttpRequest req addressInfo = runResourceT @IO do
 res <- openAndConnect addressInfo
 case res of
   Left _ -> fail "connection failed"
   Right (_, s) ->
     liftIO do
       Net.send s req
       repeatUntilNothing' (Net.recv s 1024)  BS.putStr
       S.gracefulClose s 1000

findHaskellWebsite''' :: IO S.AddrInfo
findHaskellWebsite''' = resolve "http" "www.haskell.org"

--- Custom datatypes
data Request = Request RequestLine [HeaderField] (Maybe MessageBody)

data Response = Response StatusLine [HeaderField] (Maybe MessageBody)

data RequestLine = RequestLine Method RequestTarget HttpVersion

--data Method = Get | Head | Post | Put | Delete | Connect | Options | Trace

data Method = Method BS.ByteString

data RequestTarget = RequestTarget BS.ByteString

data StatusLine = StatusLine HttpVersion StatusCode ReasonPhrase
  deriving (Eq,Show)

data StatusCode = StatusCode A.Digit A.Digit A.Digit
  deriving (Eq,Show)

data ReasonPhrase = ReasonPhrase BS.ByteString
  deriving (Eq,Show)

data HeaderField = HeaderField FieldName FieldValue

data FieldName = FieldName BS.ByteString

data FieldValue = FieldValue  BS.ByteString

data MessageBody = MessageBody LBS.ByteString

data HttpVersion = HttpVersion A.Digit A.Digit
  deriving (Eq,Show)

--exercise16 = Method "Get"
mymethod :: Method  --BS.ByteString
mymethod = Method [A.string|GET |]

reqTarget :: RequestTarget
reqTarget = RequestTarget  [A.string|/hello.txt |]

httpVersion :: HttpVersion
httpVersion = HttpVersion Digit1 Digit1   --digitOne digitOne

digitOne :: A.Digit
digitOne = A.charToDigitUnsafe A.Digit1

myRequestString :: BS.ByteString
myRequestString =
  line [A.string|Get  hello.txt HTTP/1.1|] <>
  line [A.string|Host: haskell.org|] <>
  line [A.string|Connection: close|] <>
  line [A.string||]

--data Request = Request RequestLine [HeaderField] (Maybe MessageBody)
--data RequestLine = RequestLine Method RequestTarget HttpVersion
myLine :: RequestLine
myLine = RequestLine mymethod reqTarget httpVersion

--myReq =
myHost :: HeaderField
myHost = HeaderField (FieldName [A.string|Host|]) (FieldValue [A.string|www.example.com|])

myLanguage :: HeaderField
myLanguage = HeaderField (FieldName [A.string|Accept-Language|]) (FieldValue [A.string|en, mi|])

myRequest:: Request
myRequest= Request myLine [myHost,myLanguage] Nothing

myStatusCode :: StatusCode
myStatusCode = StatusCode Digit2 Digit0 Digit0  --digitTwo digitZero digitZero

digitTwo :: A.Digit
digitTwo = A.charToDigitUnsafe A.Digit2

digitZero :: Digit
digitZero = Digit0

myStatusLine :: StatusLine
myStatusLine = StatusLine httpVersion myStatusCode (ReasonPhrase [A.string|OK|])

mycontentType :: HeaderField
mycontentType = HeaderField (FieldName [A.string|Content-Type|]) (FieldValue [A.string|text/plain; charset=us-ascii|])

mycontentLength :: HeaderField
mycontentLength = HeaderField (FieldName [A.string|Content-Length|]) (FieldValue [A.string|6|])

myRespone :: Response
myRespone = Response myStatusLine [mycontentType,mycontentLength] (Just (MessageBody [A.string|Hello!|]))

myString :: BS.ByteString
myString = [A.string|abc|]

time :: IO () -> IO ()
time action = do
  a <- Time.getCurrentTime
  action
  b <- Time.getCurrentTime
  print $ Time.diffUTCTime b a

concatWithStrict :: Int -> T.Text
concatWithStrict numberOfTimes =
  fold $ replicate numberOfTimes $ T.pack "a"

concatWithBuilder :: Int -> T.Text
concatWithBuilder numberOfTimes = LT.toStrict $ TB.toLazyText $
  fold $ replicate numberOfTimes $ TB.fromString "a"

concatSpeedText :: Int -> IO ()
concatSpeedText n = do
  dir <- getDataDir
  time $ T.writeFile (dir </> "strict.txt")
                     (concatWithStrict n)
  time $ T.writeFile (dir </> "builder.txt")
                     (concatWithBuilder n)

--encodeResponse :: Request -> BSB.Builder
encodeLineEnd :: BSB.Builder
encodeLineEnd = A.lift crlf

encodeRequest :: Request -> BSB.Builder
encodeRequest (Request requestLine headerFields bodyMaybe) =
  encodeRequestLine requestLine
  <> repeatedlyEncode
      (\x -> encodeHeaderField x <> encodeLineEnd)
      headerFields
  <> encodeLineEnd
  <> optionallyEncode encodeMessageBody bodyMaybe

encodeResponse :: Response -> BSB.Builder
encodeResponse (Response statusLine headerFields bodyMaybe) =
  encodeStatusLine statusLine
  <> repeatedlyEncode
      (\x -> encodeHeaderField x <> encodeLineEnd)
      headerFields
  <> encodeLineEnd
  <> optionallyEncode encodeMessageBody bodyMaybe

repeatedlyEncode :: (a -> BSB.Builder) -> [a] -> BSB.Builder
repeatedlyEncode = foldMap

optionallyEncode ::  (a -> BSB.Builder) -> Maybe a -> BSB.Builder
optionallyEncode = foldMap

encodeRequestLine :: RequestLine -> BSB.Builder
encodeRequestLine (RequestLine method target version) =
  encodeMethod method <> A.lift [A.Space]
  <> encodeRequestTarget target <> A.lift [A.Space]
  <> encodeHttpVersion version <> encodeLineEnd

encodeMethod :: Method -> BSB.Builder
encodeMethod (Method x) = BSB.byteString x

encodeRequestTarget :: RequestTarget -> BSB.Builder
encodeRequestTarget (RequestTarget x) = BSB.byteString x

encodeHttpVersion :: HttpVersion -> BSB.Builder
encodeHttpVersion (HttpVersion x y) =
  [A.string|HTTP/|] <> A.lift [x] <> [A.string|.|] <> A.lift [y]

encodeMessageBody :: MessageBody -> BSB.Builder
encodeMessageBody (MessageBody x) = BSB.byteString $ LBS.toStrict x

encodeHeaderField :: HeaderField -> BSB.Builder
encodeHeaderField (HeaderField (FieldName x) (FieldValue y)) =
  (BSB.byteString x) <> [A.string|:|] <> (BSB.byteString y)

encodeStatusLine :: StatusLine -> BSB.Builder
encodeStatusLine (StatusLine  version statusCode reasonPhrase) =
  encodeHttpVersion version <> A.lift [A.Space]
  <> encodeStatusCode statusCode <> A.lift [A.Space]
  <> encodeReasonPhrase reasonPhrase <> encodeLineEnd

encodeReasonPhrase :: ReasonPhrase -> BSB.Builder
encodeReasonPhrase (ReasonPhrase x) = BSB.byteString x

encodeStatusCode :: StatusCode -> BSB.Builder
encodeStatusCode (StatusCode x y z) = A.lift [x, y, z]

convertBuildToByte :: BSB.Builder -> ByteString
convertBuildToByte = LBS.toStrict . BSB.toLazyByteString

data Status = Status StatusCode ReasonPhrase

ok :: Status
ok = Status (StatusCode Digit2 Digit0 Digit0) (ReasonPhrase [A.string|OK|])

status :: Status -> StatusLine
status (Status code phrase)  = StatusLine http_1_1 code phrase

http_1_1 :: HttpVersion
http_1_1 = HttpVersion Digit1 Digit1

contentType :: FieldName
contentType = FieldName [A.string|Content-Type|]

plainAscii :: FieldValue
plainAscii = FieldValue [A.string|text/plain; charset=us-ascii|]

contentLength :: FieldName
contentLength = FieldName [A.string|Content-Length|]

asciiOk :: LBS.ByteString -> Response
asciiOk str = Response (status ok) [typ,len] (Just body)
  where typ = HeaderField contentType plainAscii
        len = HeaderField contentLength (bodyLengthValue body)
        body = MessageBody str

countHelloAscii :: Natural -> LBS.ByteString
countHelloAscii count = BSB.toLazyByteString $
  [A.string|Hello!|] <> encodeLineEnd <> case count of
    0 -> [A.string|This page has never been viewed.|]
    1 -> [A.string|This page has been viewed 1 time.|]
    _ -> [A.string|This page has been viewed.|] <>
          A.showIntegralDecimal count <> [A.string| times.|]

bodyLengthValue :: MessageBody -> FieldValue
bodyLengthValue (MessageBody x) = FieldValue (A.showIntegralDecimal (LBS.length x))

sendResponse :: Socket -> Response -> IO ()
sendResponse s r = Net.sendLazy s $ BSB.toLazyByteString $ encodeResponse r

stuckCountingServer :: IO a
stuckCountingServer = serve @IO HostAny "8000" \(s, _) -> do
  let count = 0 -- to-do!
  sendResponse s (asciiOk (countHelloAscii count))

mid :: Word8 -> Word8 -> Word8
mid x y = fromInteger $ div (f x + f y) 2
  where f = toInteger

plainUtf8 :: FieldValue
plainUtf8 = FieldValue [A.string|text/plain; charset=utf-8|]

htmlUtf8 :: FieldValue
htmlUtf8 = FieldValue [A.string|text/html; charset=utf-8|]

json :: FieldValue
json = FieldValue [A.string|application/json|]

countHelloText :: Natural -> LT.Text
countHelloText count = TB.toLazyText $
  TB.fromString "Hello! \9835\r\n" <>
   case count of
          0 -> TB.fromString "This page has never been viewed."
          1 -> TB.fromString "This page has been viewed 1 time."
          _ -> TB.fromString "This page has been viewed " <>
               TB.decimal count <> TB.fromString " times."

textOk :: LT.Text -> Response
textOk str = Response (status ok) [typ, len] (Just body)
  where
    typ = HeaderField contentType plainUtf8
    len = HeaderField contentLength (bodyLengthValue body)
    body = MessageBody (LT.encodeUtf8 str)

stuckCountingServerText :: IO a
stuckCountingServerText = serve @IO HostAny "8000" \(s, _) -> do
   let count = 0 -- to-do!
   sendResponse s (textOk (countHelloText count))

countHelloHtml :: Natural -> Html
countHelloHtml count = HTML.docType <> htmlDocument --1
  where
  htmlDocument = HTML.html $  documentMetadata <> documentBody
  documentMetadata = HTML.head titleHtml
  titleHtml = HTML.title (toHtml "My great web page") --4
  documentBody = HTML.body $  greetingHtml <> HTML.hr <> hitCounterHtml
  greetingHtml = HTML.p (toHtml "Hello! \9835")
  hitCounterHtml = HTML.p $ case count of --7
          0 -> toHtml "This page has never been viewed."
          1 -> toHtml "This page has been viewed 1 time."
          _ -> toHtml "This page has been viewed " <>
               toHtml @Natural count <> toHtml " times."

countHelloJson :: Natural -> J.Value
countHelloJson count =  toJSON (J.KeyMap.fromList [greetingJson, hitsJson]) -- 1
   where
    greetingJson = (J.Key.fromString "greeting", toJSON "Hello! \9835")
    hitsJson =  (J.Key.fromString "hits", toJSON (J.KeyMap.fromList  [numberJson, messageJson]))
    numberJson=  (J.Key.fromString "count",  toJSON count)
    messageJson = (J.Key.fromString "message",  toJSON (countHelloText count))

countHelloJson' :: Natural -> J.Value
countHelloJson' count = J.object [
  fromString "greeting" .= fromString @T.Text "Hello! \9835",
  fromString "hits" .= J.object [
    fromString "count" .= count,
    fromString "message" .= countHelloText count
  ]]

jsonOk :: J.Value -> Response
jsonOk str = Response (status ok) [typ, len] (Just body)
  where
    typ = HeaderField contentType json
    len = HeaderField contentLength (bodyLengthValue body)
    body = MessageBody (J.encode str)

htmlOk :: Html -> Response
htmlOk str = Response (status ok) [typ, len] (Just body)
  where
    typ = HeaderField contentType htmlUtf8
    len = HeaderField contentLength (bodyLengthValue body)
    body = MessageBody (renderHtml str)

stuckCountingServerHtml :: IO a
stuckCountingServerHtml = serve @IO HostAny "8000" \(s, _) -> do
   let count = 0 -- to-do!
   sendResponse s (htmlOk (countHelloHtml count))  --(textOk (countHelloText count))

increment :: TVar Natural -> STM Natural
increment hitCounter = do
  oldCount <- readTVar hitCounter
  let newCount = oldCount + 1
  writeTVar hitCounter newCount -- fancy way of mutating
  return newCount

countingServer :: IO b
countingServer = do
  hitCounter <- atomically (newTVar @Natural 0)
  serve @IO HostAny "8000" \(s, _) -> do
    count <- atomically (increment hitCounter)
    sendResponse s (textOk (countHelloText count))

incrementNotAtomic :: TVar Natural -> IO Natural
incrementNotAtomic hitCounter = do
  oldCount <- atomically $ readTVar hitCounter
  let newCount = oldCount + 1
  atomically $ writeTVar hitCounter newCount
  return newCount

testIncrement :: (TVar Natural -> IO a) -> IO Natural
testIncrement inc = do
  x <- atomically (newTVar @Natural 0)
  Async.replicateConcurrently_  10 (replicateM 1000 (inc x))
  atomically (readTVar x)

timingServer :: IO ()
timingServer = do
  fstTime <- Time.getCurrentTime
  hitTime <- atomically (newTVar @Time.UTCTime fstTime)
  serve @IO HostAny "8000" \(s, _) -> do
    curTime <- Time.getCurrentTime
    oldTime <- atomically $ readTVar hitTime
    let diff = Time.diffUTCTime curTime oldTime
    atomically $ writeTVar hitTime curTime
--    diff <- atomically (change curTime hitTime)
    sendResponse s $ textOk $ LT.pack $ show (diff)
  --    show ()

timingServer' :: IO()
timingServer' = do
  timeVar <- atomically $ newTVar @(Maybe Time.UTCTime) Nothing
  serve @IO HostAny "8000" \(s, _) -> do
    now <- Time.getCurrentTime
    diff <- atomically $ updateTime timeVar now
    sendResponse s (textOk (show diff))

updateTime :: TVar (Maybe Time.UTCTime) -> Time.UTCTime -> STM (Maybe Time.NominalDiffTime)
updateTime timeVar now = do
  previousTimeMaybe <- readTVar timeVar
  writeTVar timeVar (Just now)
  return $ Time.diffUTCTime now <$> previousTimeMaybe

  {-
  return $ case previousTimeMaybe of
    Nothing -> Nothing
    Just past -> Just $ Time.diffUTCTime now past

change :: Time.UTCTime -> TVar Time.UTCTime -> STM Time.UTCTime
change curTime hitTime = do
  oldTime <- readTVar hitTime
  let diff = Time.diffUTCTime curTime oldTime
  writeTVar hitTime diff -- fancy way of mutating
  return diff
-}

data Chunk = Chunk ChunkSize ChunkData

data ChunkSize = ChunkSize Natural

data ChunkData = ChunkData BS.ByteString

dataChunk :: ChunkData -> Chunk
dataChunk chunkData = Chunk (chunkDataSize chunkData) chunkData

chunkDataSize :: ChunkData -> ChunkSize
chunkDataSize (ChunkData bs) =
  case toIntegralSized @Int @Natural (BS.length bs) of
    Just n -> (ChunkSize n)
    Nothing -> error (T.pack "BS.length is always Natural")

encodeChunk :: Chunk -> BSB.Builder
encodeChunk (Chunk chunkSize chunkData) =
  encodeChunkSize chunkSize <> encodeLineEnd <>
  encodeChunkData chunkData <> encodeLineEnd

encodeLastChunk :: BSB.Builder
encodeLastChunk = encodeChunkSize (ChunkSize 0) <> encodeLineEnd

encodeChunkData :: ChunkData -> BSB.Builder
encodeChunkData (ChunkData x) = BSB.byteString x

encodeChunkSize :: ChunkSize -> BSB.Builder
encodeChunkSize (ChunkSize x) = A.showIntegralHexadecimal A.LowerCase x

exampleHexNumbers :: [Text]
exampleHexNumbers = map hex [0 .. 32]
  where
    hex :: Natural -> Text
    hex = A.showIntegralHexadecimal A.LowerCase

transferEncoding :: FieldName
transferEncoding = FieldName [A.string|Trasfer-Encoding|]

chunked :: FieldValue
chunked = FieldValue [A.string|chunked|]

transferEncodingChunked :: HeaderField
transferEncodingChunked = HeaderField transferEncoding chunked

encodeHeaders :: [HeaderField] -> BSB.Builder
encodeHeaders xs =
  repeatedlyEncode
    (\x -> encodeHeaderField x <> encodeLineEnd) xs
  <> encodeLineEnd

encodeTrailers :: [HeaderField] -> BSB.Builder
encodeTrailers = encodeHeaders

fileStreaming :: IO b
fileStreaming = do
  dir <- getDataDir
  serve @IO HostAny "8000" \(s, _) -> runResourceT @IO do
    (_, h) <-  binaryFileResource (dir </> "stream.txt") ReadMode
    liftIO do
     sendBSB s (encodeStatusLine (status ok))
     sendBSB s (encodeHeaders [transferEncodingChunked])
     repeatUntil (BS.hGetSome h 1024) BS.null \c ->
                 sendBSB s (encodeChunk (dataChunk (ChunkData c)))
     sendBSB s encodeLastChunk
     sendBSB s encodeLineEnd
     sendBSB s (encodeTrailers [])

sendBSB :: Socket -> BSB.Builder -> IO ()
sendBSB s b = Net.sendLazy s (BSB.toLazyByteString b)

infiniteFileStreaming :: IO b
infiniteFileStreaming = do
  dir <- getDataDir
  serve @IO HostAny "8000" \(s, _) -> runResourceT @IO do
    (_, h) <-  binaryFileResource (dir </> "stream.txt") ReadMode
    liftIO $ forever $ do
     threadDelay 10000
     sendBSB s (encodeStatusLine (status ok))
     sendBSB s (encodeHeaders [transferEncodingChunked])
     repeatUntil (BS.hGetSome h 1024) BS.null \c ->
                 sendBSB s (encodeChunk (dataChunk (ChunkData c)))
     sendBSB s encodeLastChunk
     sendBSB s encodeLineEnd
     sendBSB s (encodeTrailers [])

data MaxChunkSize = MaxChunkSize Int

sendStreamingResponse :: Socket -> StreamingResponse -> IO ()
sendStreamingResponse s r = runListT @IO do
  bs <- encodeStreamingResponse r
  Net.send s bs
{-
fileStreaming2 = do
  dir <- getDataDir
  let fp = dir </> "stream.txt"
  serve @IO HostAny \(s, _) -> runResourceT @IO do
    (_, h) <- binaryFileResource (dir </> "stream.txt") ReadMode
    let r = hStreamingResponse h (MaxChunkSize 1024)
    liftIO (sendStreamingResponse s r)
-}
data StreamingResponse = StreamingResponse StatusLine [HeaderField] (Maybe ChunkedBody)

data ChunkedBody = ChunkedBody (ListT IO Chunk)

printMenu :: [(String,String)] -> IO ()
printMenu meals = runListT @IO do
  (entree, side) <- ListT.select @[] meals
  liftIO (IO.putStrLn (entree <|> " and " <|> side))

haskellCafe :: [(String,String)]
haskellCafe = do
  entree <- ["chicken","fish"]
  side <- ["soup","salad"]
  return (entree,side)

metronome :: IO ()
metronome = runListT @IO do
  ListT.select @[] (repeat ())
  liftIO (IO.putStrLn "tick")
  liftIO (threadDelay 1000000)

hStreamingResponse :: Handle -> MaxChunkSize -> StreamingResponse
hStreamingResponse h maxChunkSize =
      StreamingResponse statusLine headers (Just body)
  where
      statusLine = status ok                       -- 1
      headers = [transferEncodingChunked]          -- 2
      body = chunkedBody (hChunks h maxChunkSize)

hChunks :: Handle -> MaxChunkSize -> ListT IO BS.ByteString
hChunks h maxChunkSize = ListT.takeWhile (not . BS.null) (hInfiniteChunks h maxChunkSize)

hInfiniteChunks :: Handle -> MaxChunkSize -> ListT IO BS.ByteString
hInfiniteChunks h (MaxChunkSize mcs) = do
  ListT.select @[] (repeat ())
  liftIO (BS.hGetSome h mcs)

chunkedBody :: ListT IO ByteString -> ChunkedBody
chunkedBody = ChunkedBody . fmap (dataChunk .  ChunkData)
{-
chunkedBody xs = ChunkedBody dy
  bs <- xs
  return (dataChunk (ChunkData bs))
-}
encodeStreamingResponse :: StreamingResponse -> ListT IO BS.ByteString
encodeStreamingResponse (StreamingResponse statusLine headers bodyMaybe) =
  asum
    [
      selectChunk (encodeStatusLine statusLine)
      ,selectChunk (encodeHeaders headers)
      , do
          ChunkedBody chunks <- ListT.select @Maybe bodyMaybe
          asum
            [
              do
                chunk <- chunks
                selectChunk (encodeChunk chunk)
              , selectChunk encodeLastChunk
              , selectChunk (encodeTrailers [])
            ]
    ]

selectChunk :: BSB.Builder -> ListT IO BS.ByteString
selectChunk b = ListT.select @[] (LBS.toChunks (BSB.toLazyByteString b))

listUntilIO :: IO chunk -> (chunk -> Bool) -> ListT IO chunk
listUntilIO action isEnd = ListT.takeWhile (not . isEnd) (listForeverIO action)

listForeverIO :: IO a -> ListT IO a
listForeverIO action = do
  ListT.select @[]  (repeat ())
  liftIO action

{-
hChunks :: Handle -> MaxChunkSize -> ListT IO BS.ByteString
hChunks h maxChunkSize = ListT.takeWhile (not . BS.null) (hInfiniteChunks h maxChunkSize)
-}
hChunks'  :: Handle -> MaxChunkSize -> ListT IO BS.ByteString
hChunks' h (MaxChunkSize mcs) = listUntilIO (BS.hGetSome h mcs) BS.null

copyGreetingStream :: IO ()
copyGreetingStream = runResourceT @IO do
  dir <- liftIO getDataDir
  (_, h1) <-
        binaryFileResource (dir </> "greeting.txt") ReadMode
  (_, h2) <-
        binaryFileResource (dir </> "greeting2.txt") WriteMode
  liftIO (hCopy h1 h2)

hCopy :: Handle -> Handle -> IO ()
hCopy source destination = runListT @IO do
  chunk <- hChunks' source (MaxChunkSize 1024)
  liftIO $ BS.hPutStr destination chunk

fileCopyMany :: FilePath -> [FilePath] -> IO ()
fileCopyMany source destinations = runResourceT @IO do
   (_, hSource) <- binaryFileResource source ReadMode
   hDestinations <- forM destinations \fp -> do
      (_, h) <- binaryFileResource fp WriteMode
      return h
   liftIO (hCopyMany hSource hDestinations)

hCopyMany :: Handle -> [Handle] -> IO ()
hCopyMany source destinations = runListT @IO do
  chunk <- hChunks' source (MaxChunkSize 1024)
  destination  <- ListT.select @[] destinations
  liftIO $ BS.hPutStr destination chunk
  --liftIO $ hCopy source destination

countString :: BS.ByteString
countString = [A.string|one-two-three-four|]

takeTwoWords :: Parser (BS.ByteString, BS.ByteString)
takeTwoWords = do
  a <- P.takeWhile A.isLetter
  _ <- P.string (A.lift [A.HyphenMinus])
  b <- P.takeWhile A.isLetter
  return (a, b)

spaceParser :: Parser BS.ByteString
spaceParser = P.string (A.lift [A.Space])

lineEndParser :: Parser BS.ByteString
lineEndParser = P.string (A.lift crlf)

requestLineParser :: Parser RequestLine
requestLineParser = do
  method <- methodParser <?> "Method"
  _ <- spaceParser
        <|> fail "Method should be followed by a space"
  requestTarget <- requestTargetParser <?> "Target"
  _ <- spaceParser
        <|> fail "Target should be followed by a space"
  httpVersion <- httpVersionParser <?> "Version"
  _ <- lineEndParser
        <|> fail "Target should be followed by a space"
  return (RequestLine method requestTarget httpVersion)

methodParser :: Parser Method
methodParser = do
  x <- tokenParser
  return (Method x)

tcharSymbols :: [Word8]
tcharSymbols = A.lift [A.ExclamationMark,A.NumberSign,A.DollarSign,A.PercentSign, A.Ampersand, A.Apostrophe, A.Asterisk
                      , A.PlusSign, A.HyphenMinus, A.FullStop, A.Caret, A.Underscore, A.GraveAccent, A.VerticalLine, A.Tilde
                      ]

isTchar :: Word8 -> Bool
isTchar c = elem c tcharSymbols || A.isDigit c || A.isLetter c

tokenParser :: Parser BS.ByteString
tokenParser = do
  token <- P.takeWhile isTchar
  when (BS.null token) (fail "tchar expected")
  return token

requestTargetParser :: Parser RequestTarget
requestTargetParser = do
  x <- P.takeWhile A.isVisible
  when (BS.null x) (fail "vchar expected")
  return (RequestTarget x)

httpVersionParser :: Parser HttpVersion
httpVersionParser = do
  _ <- P.string [A.string|HTTP|] <|> fail "Should begin with HTTP"
  _ <- P.string (A.lift [A.Slash]) <|> fail "HTTP should be followed by a slash"
  x <- digitParser <?> "First digit"
  _ <- P.string (A.lift [A.FullStop]) <|> fail "First Digit should be followed by a dot"
  y <- digitParser <?> "Second digit"
  return (HttpVersion x y)

digitParser :: Parser A.Digit
digitParser = do
  x <- P.anyWord8
  unless (A.isDigit x) (fail "0-9 expected")
  return (A.word8ToDigitUnsafe x)
{-
digitParser :: Parser A.Digit
digitParser = do
  x <- P.anyWord8
  case

-}
readRequestLine :: MaxChunkSize -> Socket -> IO RequestLine
readRequestLine (MaxChunkSize mcs) s = do
  result <- P.parseWith (S.recv s mcs)
              requestLineParser BS.empty
  case P.eitherResult result of
    Left errorMessage -> fail errorMessage
    Right requestLine -> return requestLine

resourceServer :: IO ()
resourceServer = do
  dir <- getDataDir
  let resources = resourceMap dir
  let maxChunkSize = MaxChunkSize 1024
  serve @IO HostAny "8000" \(s, _) ->
    serveResourceOnce resources maxChunkSize s

serveResourceOnce :: ResourceMap -> MaxChunkSize -> Socket -> IO ()
serveResourceOnce resources maxChunkSize s = runResourceT @IO do
  RequestLine _ target _ <- liftIO (readRequestLine maxChunkSize s)
  filePath <- liftIO (getTargetFilePath resources target)
  (_ , h) <- binaryFileResource filePath ReadMode
  let r = hStreamingResponse h maxChunkSize
  liftIO (sendStreamingResponse s r )

data ResourceMap = ResourceMap (Map T.Text FilePath)

resourceMap :: FilePath -> ResourceMap
resourceMap dir = ResourceMap $ Map.fromList
  [
    (T.pack "/stream", dir </> "stream.txt")
    ,(T.pack "/read", dir </> "read.txt")
  ]

getTargetFilePath :: MonadFail m =>
                           ResourceMap -> RequestTarget -> m FilePath
getTargetFilePath rs target =
  case targetFilePathMaybe rs target of
    Nothing -> fail "not found"
    Just fp -> return fp

targetFilePathMaybe :: ResourceMap -> RequestTarget -> Maybe FilePath
targetFilePathMaybe (ResourceMap rs) (RequestTarget target) = do
  resource <- A.convertStringMaybe target
  Map.lookup resource rs

-- 13.6
parenParser :: Parser BS.ByteString
parenParser = do
  _ <- P.string (A.lift [A.LeftParenthesis])
  x <- P.takeWhile (/= A.lift A.RightParenthesis)
  _ <- P.string (A.lift [A.RightParenthesis])
  return x

statusLineParser :: Parser StatusLine
statusLineParser = do
  version <- httpVersionParser
  _ <- spaceParser
  statusCode <- statusCodeParser
  _ <- spaceParser
  reasonPhrase <- reasonPhraseParser
  _ <- lineEndParser
  return (StatusLine version statusCode reasonPhrase)

statusCodeParser :: Parser StatusCode
statusCodeParser = do
  d1 <- digitParser
  d2 <- digitParser
  d3 <- digitParser
  return $ StatusCode d1 d2 d3

reasonPhraseParser :: Parser ReasonPhrase
reasonPhraseParser = do
  phrase <- P.takeWhile \x ->
    elem x (A.lift [A.HorizontalTab, A.Space] :: [Word8])
    || A.isVisible x || x >= 0x80
  return (ReasonPhrase phrase)

statusLineParserTest :: StatusLine -> Maybe String
statusLineParserTest x =
  case result of
    Left e -> Just e
    Right x'
     | x /= x' -> Just (show x')
     | otherwise -> Nothing
  where
    encodedStatusLine = encodeStatusLine x
    bs = LBS.toStrict $  BSB.toLazyByteString encodedStatusLine
    result = P.parseOnly (statusLineParser <* P.endOfInput) bs

stringParser :: BS.ByteString -> Parser BS.ByteString
stringParser bs = do
  xs <- replicateM (BS.length bs) P.anyWord8
  when (BS.pack xs /= bs) (fail "string")
  return bs